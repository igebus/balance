-- This file should undo anything in `up.sql`

DROP TABLE invites;
DROP TABLE transactions;
DROP TABLE relations;
DROP TABLE users;
DROP TABLE trips;
