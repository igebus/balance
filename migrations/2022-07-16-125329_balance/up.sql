-- Your SQL goes here

CREATE TABLE IF NOT EXISTS trips (
    trip_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    title TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    admin BOOLEAN NOT NULL,

    UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS relations (
    trip_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,

    FOREIGN KEY (trip_id) REFERENCES trips(trip_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    PRIMARY KEY (user_id, trip_id)
);

CREATE TABLE IF NOT EXISTS transactions (
    transaction_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    trip_id INTEGER NOT NULL,
    sender_id INTEGER NOT NULL,
    recipient_id INTEGER NOT NULL,
    title TEXT NOT NULL,
    amount DOUBLE NOT NULL,
    created_at TEXT NOT NULL,
    pending BOOLEAN NOT NULL,
    accepted BOOLEAN NOT NULL,

    FOREIGN KEY (trip_id) REFERENCES trips(trip_id),
    FOREIGN KEY (sender_id) REFERENCES users(user_id),
    FOREIGN KEY (recipient_id) REFERENCES users(user_id)
);

CREATE TABLE IF NOT EXISTS invites (
    invite_id TEXT PRIMARY KEY NOT NULL
);
