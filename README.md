# Description

_Balance_ is simple web application written in Rust that can be used to help with billing between friends during trips (or any form of tracking and maintaining amount of debts / refunds for group of people).

# Configuration / Deployment
* application configuration is stored in `.env`
* it may be necessary to run `sqlite` migrations, in that case install `diesel_cli` using `cargo install diesel_cli --no-default-features --features "sqlite"` and then run migrations `diesel database reset`
* to run, issue command `cargo run --release`

# Usage
* app instance is binded to `localhost:8080` by default
* you have to sing up - first account has admin privileges
* then log in - you have access to admin panel from where you can manage whole local instance
