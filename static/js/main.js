function sendPostRequest(action, payload) {
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            if(xhr.response)
                alert(xhr.response);
            else
                window.location.reload();
        }
    }

    xhr.open("POST", window.location.pathname, true);
    xhr.setRequestHeader("Content-type", "application/json");
    let parameters = JSON.stringify({"action":action,"payload":payload});
    xhr.send(parameters);
}

function submitForm(e) {
    let form = e.target;

    let data = new FormData(form);
    let payload = {};
    for(let entry of data.entries()) {
        let [entryName, entryValue] = entry;

        if(entryName.endsWith("[]")) {
            entryName = entryName.slice(0,-2);

            if(entryName in payload == false)
                payload[entryName] = [];

            payload[entryName].push(entryValue);
        }
        else
            payload[entryName] = entryValue;
    }

    sendPostRequest(form.name, payload);
    e.preventDefault();
}

function checkAllCheckboxes(e) {
    let source = e.target;

    let checkboxes = document.getElementsByClassName(source.className);

    for(let checkbox of checkboxes) {
        if(checkbox != source && checkbox.type === "checkbox")
            checkbox.checked = true;
    }
}

for(let form of document.forms) {
    form.addEventListener("submit", submitForm, true);
}

for(let button of document.getElementsByName("check_all")) {
    if(button.type === "button")
        button.addEventListener("click", checkAllCheckboxes, true);
}
