#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate lazy_static;

pub mod config;
pub mod errors;
pub mod identity;
pub mod locations;
pub mod models;
pub mod schema;

use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::{web, web::Data, App, HttpResponse, HttpServer, Responder};
use diesel::prelude::*;
use diesel_migrations::embed_migrations;
use rust_embed::RustEmbed;
use tera::Tera;

use config::CONFIG;
use locations::*;

embed_migrations!("migrations/");

#[derive(RustEmbed)]
#[folder = "static/"]
struct Assets;

#[actix_web::get("/static/{_:.*}")]
async fn handle_static(path: web::Path<String>) -> impl Responder {
    match Assets::get(path.as_str()) {
        Some(content) => {
            let content_type = mime_guess::from_path(path.as_str()).first_or_octet_stream();
            HttpResponse::Ok()
                .content_type(content_type)
                .body(content.data.into_owned())
        }
        None => HttpResponse::NotFound().finish(),
    }
}

#[derive(RustEmbed)]
#[folder = "templates/"]
struct Templates;

pub fn establish_db_connection() -> SqliteConnection {
    SqliteConnection::establish(&CONFIG.database_url)
        .expect(&format!("Error connecting to {}", CONFIG.database_url))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let db_connection = establish_db_connection();
    embedded_migrations::run(&db_connection).unwrap();

    HttpServer::new(|| {
        let mut tera = Tera::default();
        tera.add_raw_templates(Templates::iter().map(|filename| {
            (
                filename.to_string(),
                String::from_utf8(Templates::get(&filename).unwrap().data.to_vec()).unwrap(),
            )
        }))
        .unwrap();

        App::new()
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&[0; 32])
                    .name("auth-cookie")
                    .secure(false),
            ))
            .app_data(Data::new(tera))
            .service(handle_static)
            .route("/", web::get().to(index::index))
            .route("/signup", web::get().to(signup::view))
            .route("/signup", web::post().to(signup::handle))
            .route("/login", web::get().to(login::view))
            .route("/login", web::post().to(login::handle))
            .route("/logout", web::get().to(logout::logout))
            .route("/admin", web::get().to(admin::view))
            .route("/admin", web::post().to(admin::handle))
            .route("/dashboard", web::get().to(dashboard::view))
            .route("/dashboard", web::post().to(dashboard::handle))
    })
    .bind(CONFIG.socket_addr.as_str())?
    .run()
    .await
}
