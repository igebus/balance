table! {
    invites (invite_id) {
        invite_id -> Text,
    }
}

table! {
    relations (trip_id, user_id) {
        trip_id -> Integer,
        user_id -> Integer,
    }
}

table! {
    transactions (transaction_id) {
        transaction_id -> Integer,
        trip_id -> Integer,
        sender_id -> Integer,
        recipient_id -> Integer,
        title -> Text,
        amount -> Double,
        created_at -> Text,
        pending -> Bool,
        accepted -> Bool,
    }
}

table! {
    trips (trip_id) {
        trip_id -> Integer,
        title -> Text,
    }
}

table! {
    users (user_id) {
        user_id -> Integer,
        username -> Text,
        password -> Text,
        admin -> Bool,
    }
}

joinable!(relations -> trips (trip_id));
joinable!(relations -> users (user_id));
joinable!(transactions -> trips (trip_id));

allow_tables_to_appear_in_same_query!(invites, relations, transactions, trips, users,);
