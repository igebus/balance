use super::schema::*;
use argon2::{self, Config};
use diesel::{Insertable, Queryable};
use rand::{distributions::Alphanumeric, Rng};
use serde::{Deserialize, Serialize};
use serde_aux::prelude::*;

#[derive(Serialize, Queryable)]
pub struct Trip {
    pub trip_id: i32,
    pub title: String,
}

#[derive(Deserialize, Insertable)]
#[table_name = "trips"]
pub struct NewTrip {
    pub title: String,
}

#[derive(Deserialize)]
pub struct IdTrip {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub trip_id: i32,
}

#[derive(Queryable, Serialize)]
pub struct User {
    pub user_id: i32,
    pub username: String,
    pub password: String,
    pub admin: bool,
}

#[derive(Deserialize, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub password: String,
    pub admin: bool,
}

impl NewUser {
    pub fn new(username: String, password: String, admin: bool) -> Self {
        let salt: String = rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(32)
            .map(char::from)
            .collect();

        let hash =
            argon2::hash_encoded(password.as_bytes(), salt.as_bytes(), &Config::default()).unwrap();

        NewUser {
            username,
            password: hash,
            admin,
        }
    }
}

#[derive(Deserialize)]
pub struct IdUser {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub user_id: i32,
}

#[derive(Deserialize)]
pub struct SignupUser {
    pub username: String,
    pub password: String,
    pub invite_id: String,
}

#[derive(Deserialize)]
pub struct LoginUser {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Insertable, Queryable)]
#[table_name = "relations"]
pub struct Relation {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub user_id: i32,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub trip_id: i32,
}

#[derive(Serialize, Queryable)]
pub struct Transaction {
    pub transaction_id: i32,
    pub trip_id: i32,
    pub sender_id: i32,
    pub recipient_id: i32,
    pub title: String,
    pub amount: f64,
    pub created_at: String,
    pub pending: bool,
    pub accepted: bool,
}

#[derive(Deserialize, Insertable)]
#[table_name = "transactions"]
pub struct NewTransaction {
    pub trip_id: i32,
    pub sender_id: i32,
    pub recipient_id: i32,
    pub title: String,
    pub amount: f64,
    pub created_at: String,
    pub pending: bool,
    pub accepted: bool,
}

#[derive(Deserialize)]
pub struct IdTransaction {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub transaction_id: i32,
}

#[derive(Deserialize)]
pub struct NewTransactionWrapper {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub trip_id: i32,
    pub sender_id_strings: Vec<String>,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub recipient_id: i32,
    pub title: String,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub amount: f64,
}

#[derive(Debug, Serialize, Deserialize, Insertable, Queryable)]
#[table_name = "invites"]
pub struct Invite {
    pub invite_id: String,
}

#[derive(Serialize)]
pub struct Balance {
    pub user_id: i32,
    pub sum: f64,
}

#[derive(Deserialize)]
#[serde(tag = "action", content = "payload")]
pub enum ActionWrapper {
    #[serde(rename = "add_invite")]
    AddInvite(Invite),
    #[serde(rename = "delete_invite")]
    DeleteInvite(Invite),
    #[serde(rename = "add_trip")]
    AddTrip(NewTrip),
    #[serde(rename = "delete_trip")]
    DeleteTrip(IdTrip),
    #[serde(rename = "delete_user")]
    DeleteUser(IdUser),
    #[serde(rename = "add_relation")]
    AddRelation(Relation),
    #[serde(rename = "delete_relation")]
    DeleteRelation(Relation),
    #[serde(rename = "toggle_admin")]
    ToggleAdmin(IdUser),
    #[serde(rename = "add_transactions")]
    AddTransactions(NewTransactionWrapper),
    #[serde(rename = "accept_transaction")]
    AcceptTransaction(IdTransaction),
    #[serde(rename = "reject_transaction")]
    RejectTransaction(IdTransaction),
    #[serde(rename = "delete_transaction")]
    DeleteTransaction(IdTransaction),
}
