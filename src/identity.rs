use crate::errors::*;
use crate::models::User;
use crate::schema;
use actix_identity::Identity;
use diesel::prelude::*;

pub fn verify(id: &Identity) -> Result<User, IdentityError> {
    use schema::users;

    let user_id = id
        .identity()
        .ok_or(IdentityError::IdentityInvalid)?
        .parse::<i32>()
        .map_err(|_| return IdentityError::IdentityInvalid)?;

    let db_connection = crate::establish_db_connection();
    let user = users::table
        .find(user_id)
        .first::<User>(&db_connection)
        .map_err(|_| return IdentityError::UserNotFound)?;

    Ok(user)
}

pub fn verify_admin(id: &Identity) -> Result<User, IdentityError> {
    match verify(id) {
        Ok(u) if u.admin => Ok(u),
        Ok(_) => Err(IdentityError::UserNotAdmin),
        Err(e) => Err(e),
    }
}

pub fn verify_password(expected: &str, entered: &str) -> Result<bool, ServerError> {
    Ok(argon2::verify_encoded(expected, entered.as_bytes())?)
}
