use actix_web::HttpResponse;

pub enum IdentityError {
    IdentityInvalid,
    UserNotFound,
    UserNotAdmin,
}

#[derive(Debug)]
pub enum ServerError {
    Argon2Error,
    DieselError,
    UserError(String),
}

impl ServerError {
    pub fn new(msg: &str) -> ServerError {
        ServerError::UserError(msg.to_string())
    }
}

impl std::fmt::Display for ServerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Test")
    }
}

impl actix_web::error::ResponseError for ServerError {
    fn error_response(&self) -> HttpResponse {
        match self {
            ServerError::Argon2Error => HttpResponse::InternalServerError().json("Argon2 error"),
            ServerError::DieselError => HttpResponse::InternalServerError().json("Diesel error"),
            ServerError::UserError(data) => HttpResponse::InternalServerError().json(data),
        }
    }
}

impl From<diesel::result::Error> for ServerError {
    fn from(err: diesel::result::Error) -> ServerError {
        match err {
            diesel::result::Error::NotFound => ServerError::new("Diesel error: not found"),
            diesel::result::Error::DatabaseError(err, _) => match err {
                diesel::result::DatabaseErrorKind::UniqueViolation => {
                    ServerError::new("Diesel error: unique violation")
                }
                _ => ServerError::DieselError,
            },
            _ => ServerError::DieselError,
        }
    }
}

impl From<argon2::Error> for ServerError {
    fn from(_: argon2::Error) -> ServerError {
        ServerError::Argon2Error
    }
}
