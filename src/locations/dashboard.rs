use actix_identity::Identity;
use actix_web::{web, HttpResponse, Responder};
use diesel::{dsl::*, prelude::*};
use tera::{Context, Tera};

use crate::{errors::*, identity, models::*, schema};

use super::login;

pub const PATH: &str = "/dashboard";
pub const RESOURCE: &str = "dashboard.html";

pub fn redirect() -> HttpResponse {
    return HttpResponse::Found()
        .append_header(("Location", PATH))
        .finish();
}

pub async fn view(tera: web::Data<Tera>, id: Identity) -> Result<impl Responder, ServerError> {
    let my_user = match identity::verify(&id) {
        Ok(user) => user,
        Err(_) => {
            id.forget();
            return Ok(login::redirect());
        }
    };

    let rendered = tera
        .render(RESOURCE, &dashboard_context(&my_user)?)
        .unwrap();
    Ok(HttpResponse::Ok().body(rendered))
}

fn dashboard_context(user: &User) -> Result<Context, ServerError> {
    use schema::{relations, transactions, trips, users};

    let db_connection = crate::establish_db_connection();

    let all_my_trips = trips::table
        .inner_join(relations::table)
        .select(trips::all_columns)
        .filter(relations::user_id.eq(user.user_id))
        .load::<Trip>(&db_connection)?;

    let mut my_trips_info: Vec<(
        Trip,
        Vec<User>,
        Vec<Balance>,
        Vec<Transaction>,
        Vec<Transaction>,
    )> = Vec::with_capacity(all_my_trips.len());

    for my_trip in all_my_trips {
        let my_trip_users = users::table
            .inner_join(relations::table)
            .select(users::all_columns)
            .filter(relations::trip_id.eq(my_trip.trip_id))
            .filter(not(users::user_id.eq(user.user_id)))
            .load::<User>(&db_connection)?;

        let mut my_trip_balances: Vec<Balance> = Vec::with_capacity(my_trip_users.len());

        for other_user in &my_trip_users {
            let local_outcomes = transactions::table
                .filter(transactions::trip_id.eq(my_trip.trip_id))
                .filter(transactions::pending.eq(false))
                .filter(transactions::accepted.eq(true))
                .filter(transactions::sender_id.eq(user.user_id))
                .filter(transactions::recipient_id.eq(other_user.user_id))
                .select(transactions::amount)
                .load::<f64>(&db_connection)?;

            let local_incomes = transactions::table
                .filter(transactions::trip_id.eq(my_trip.trip_id))
                .filter(transactions::pending.eq(false))
                .filter(transactions::accepted.eq(true))
                .filter(transactions::sender_id.eq(other_user.user_id))
                .filter(transactions::recipient_id.eq(user.user_id))
                .select(transactions::amount)
                .load::<f64>(&db_connection)?;

            my_trip_balances.push(Balance {
                user_id: other_user.user_id,
                sum: local_incomes.iter().sum::<f64>() - local_outcomes.iter().sum::<f64>(),
            });
        }

        let my_trip_pending_transactions = transactions::table
            .filter(transactions::sender_id.eq(user.user_id))
            .or_filter(transactions::recipient_id.eq(user.user_id))
            .filter(transactions::trip_id.eq(my_trip.trip_id))
            .filter(transactions::pending.eq(true))
            .load::<Transaction>(&db_connection)?;

        let my_trip_processed_transactions = transactions::table
            .filter(transactions::sender_id.eq(user.user_id))
            .or_filter(transactions::recipient_id.eq(user.user_id))
            .filter(transactions::trip_id.eq(my_trip.trip_id))
            .filter(transactions::pending.eq(false))
            .load::<Transaction>(&db_connection)?;

        my_trips_info.push((
            my_trip,
            my_trip_users,
            my_trip_balances,
            my_trip_pending_transactions,
            my_trip_processed_transactions,
        ));
    }

    let mut context = Context::new();
    context.insert("my_user", &user);
    context.insert("my_trips_info", &my_trips_info);

    Ok(context)
}

pub async fn handle(
    data: web::Json<ActionWrapper>,
    id: Identity,
) -> Result<impl Responder, ServerError> {
    use schema::{relations, transactions};

    let my_user = match identity::verify(&id) {
        Ok(user) => user,
        Err(_) => {
            id.forget();
            return Ok(login::redirect());
        }
    };

    let db_connection = crate::establish_db_connection();

    match data.0 {
        ActionWrapper::AddTransactions(ts) => {
            relations::table
                .filter(relations::user_id.eq(my_user.user_id))
                .filter(relations::trip_id.eq(ts.trip_id))
                .first::<Relation>(&db_connection)?;

            if ts.recipient_id != my_user.user_id || ts.title == "" || ts.amount <= 0.0 {
                return Err(ServerError::new("Invalid transaction"));
            }

            for current_sender_id_string in ts.sender_id_strings {
                let current_sender_id = current_sender_id_string
                    .parse::<i32>()
                    .map_err(|_| ServerError::new("Invalid transaction"))?;

                let t = NewTransaction {
                    trip_id: ts.trip_id,
                    sender_id: current_sender_id,
                    recipient_id: ts.recipient_id,
                    title: ts.title.clone(),
                    amount: ts.amount,
                    created_at: chrono::offset::Local::now().to_rfc3339(),
                    pending: true,
                    accepted: false,
                };

                diesel::insert_into(transactions::table)
                    .values(&t)
                    .execute(&db_connection)?;
            }
        }
        ActionWrapper::AcceptTransaction(t) => {
            transactions::table
                .find(t.transaction_id)
                .filter(transactions::pending.eq(true))
                .filter(transactions::sender_id.eq(my_user.user_id))
                .first::<Transaction>(&db_connection)?;

            diesel::update(transactions::table.find(t.transaction_id))
                .set((
                    transactions::pending.eq(false),
                    transactions::accepted.eq(true),
                ))
                .execute(&db_connection)?;
        }
        ActionWrapper::RejectTransaction(t) => {
            transactions::table
                .find(t.transaction_id)
                .filter(transactions::pending.eq(true))
                .filter(transactions::sender_id.eq(my_user.user_id))
                .first::<Transaction>(&db_connection)?;

            diesel::update(transactions::table.find(t.transaction_id))
                .set((
                    transactions::pending.eq(false),
                    transactions::accepted.eq(false),
                ))
                .execute(&db_connection)?;
        }
        ActionWrapper::DeleteTransaction(t) => {
            diesel::delete(
                transactions::table
                    .find(t.transaction_id)
                    .filter(transactions::recipient_id.eq(my_user.user_id))
                    .filter(transactions::pending.eq(true)),
            )
            .execute(&db_connection)?;
        }
        _ => {}
    };

    Ok(HttpResponse::Ok().finish())
}
