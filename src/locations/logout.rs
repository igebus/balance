use actix_identity::Identity;
use actix_web::Responder;

use super::index;

pub const PATH: &str = "/logout";

pub async fn logout(id: Identity) -> impl Responder {
    id.forget();
    index::redirect()
}
