use actix_identity::Identity;
use actix_web::{web, HttpResponse, Responder};
use diesel::prelude::*;
use tera::{Context, Tera};

use crate::{errors::*, identity, models::*, schema};

use super::dashboard;

pub const PATH: &str = "/login";
pub const RESOURCE: &str = "login.html";

pub fn redirect() -> HttpResponse {
    return HttpResponse::Found()
        .append_header(("Location", PATH))
        .finish();
}

pub async fn view(tera: web::Data<Tera>, id: Identity) -> impl Responder {
    if identity::verify(&id).is_ok() {
        return dashboard::redirect();
    }
    let rendered = tera.render(RESOURCE, &Context::new()).unwrap();
    HttpResponse::Ok().body(rendered)
}

pub async fn handle(
    tera: web::Data<Tera>,
    data: web::Form<LoginUser>,
    id: Identity,
) -> Result<impl Responder, ServerError> {
    use schema::users::dsl::*;

    if identity::verify(&id).is_ok() {
        return Ok(dashboard::redirect());
    }

    let db_connection = crate::establish_db_connection();

    let user = users
        .filter(username.eq(&data.username))
        .first::<User>(&db_connection);

    match user {
        Ok(u) => {
            if identity::verify_password(&u.password, &data.password)? {
                id.remember(u.user_id.to_string());
                Ok(dashboard::redirect())
            } else {
                Ok(invalid_login(tera, "Incorrect password."))
            }
        }
        Err(_) => Ok(invalid_login(tera, "User doesn't exist.")),
    }
}

fn invalid_login(tera: web::Data<Tera>, feedback: &str) -> HttpResponse {
    let mut context = Context::new();
    context.insert("feedback", &feedback);
    let rendered = tera.render(RESOURCE, &context).unwrap();
    HttpResponse::Ok().body(rendered)
}
