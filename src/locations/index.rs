use actix_web::{web, HttpResponse, Responder};
use tera::{Context, Tera};

pub const PATH: &str = "/";
pub const RESOURCE: &str = "index.html";

pub fn redirect() -> HttpResponse {
    return HttpResponse::Found()
        .append_header(("Location", PATH))
        .finish();
}

pub async fn index(tera: web::Data<Tera>) -> impl Responder {
    let rendered = tera.render(RESOURCE, &Context::new()).unwrap();
    HttpResponse::Ok().body(rendered)
}
