use actix_identity::Identity;
use actix_web::{web, HttpResponse, Responder};
use diesel::prelude::*;
use tera::{Context, Tera};

use crate::{errors::*, identity, locations::login, models::*, schema};

use super::dashboard;

pub const PATH: &str = "/signup";
pub const RESOURCE: &str = "signup.html";

pub async fn view(tera: web::Data<Tera>, id: Identity) -> impl Responder {
    if identity::verify(&id).is_ok() {
        return dashboard::redirect();
    }
    let rendered = tera.render(RESOURCE, &Context::new()).unwrap();
    HttpResponse::Ok().body(rendered)
}

pub async fn handle(
    tera: web::Data<Tera>,
    data: web::Form<SignupUser>,
    id: Identity,
) -> Result<impl Responder, ServerError> {
    use schema::invites::dsl::*;
    use schema::users;
    use schema::users::dsl::*;

    if identity::verify(&id).is_ok() {
        return Ok(dashboard::redirect());
    }

    let db_connection = crate::establish_db_connection();

    let first_signup = {
        let user_count: i64 = users.count().get_result(&db_connection)?;
        user_count == 0
    };

    let invite = invites
        .find(&data.invite_id)
        .first::<Invite>(&db_connection);

    if let (Err(err), false) = (invite, first_signup) {
        return match err {
            diesel::result::Error::NotFound => Ok(invalid_signup(tera, "Invalid invite ID")),
            _ => Err(ServerError::DieselError),
        };
    }

    if let Some(feedback) = precheck_signup_credentials(&data) {
        return Ok(invalid_signup(tera, feedback));
    }

    let new_user = NewUser::new(data.username.clone(), data.password.clone(), first_signup);
    let result = diesel::insert_into(users::table)
        .values(&new_user)
        .execute(&db_connection);

    if let Err(err) = result {
        return match err {
            diesel::result::Error::DatabaseError(_, _) => {
                Ok(invalid_signup(tera, "User already exists."))
            }
            _ => Err(ServerError::DieselError),
        };
    };

    Ok(login::redirect())
}

fn invalid_signup(tera: web::Data<Tera>, feedback: &str) -> HttpResponse {
    let mut context = Context::new();
    context.insert("feedback", feedback);
    let rendered = tera.render(RESOURCE, &context).unwrap();
    HttpResponse::Ok().body(rendered)
}

fn precheck_signup_credentials(data: &web::Form<SignupUser>) -> Option<&str> {
    if !(2..=32).contains(&data.username.len()) {
        return Some("Username length should be between 2 and 32 characters.");
    }
    if !(8..=256).contains(&data.password.len()) {
        return Some("Password length should be between 8 and 256 characters.");
    }
    None
}
