use actix_identity::Identity;
use actix_web::{web, HttpResponse, Responder};
use diesel::prelude::*;
use tera::{Context, Tera};

use crate::{errors::*, identity, models::*, schema};

use super::{dashboard, login};

pub const PATH: &str = "/admin";
pub const RESOURCE: &str = "admin.html";

pub async fn view(tera: web::Data<Tera>, id: Identity) -> Result<impl Responder, ServerError> {
    let my_user = match identity::verify_admin(&id) {
        Ok(user) => user,
        Err(IdentityError::UserNotAdmin) => {
            return Ok(dashboard::redirect());
        }
        Err(_) => {
            id.forget();
            return Ok(login::redirect());
        }
    };

    let rendered = tera.render(RESOURCE, &admin_context(&my_user)?).unwrap();
    Ok(HttpResponse::Ok().body(rendered))
}

fn admin_context(admin_user: &User) -> Result<Context, ServerError> {
    use schema::invites::dsl::*;
    use schema::trips::dsl::*;
    use schema::users::dsl::*;
    use schema::{relations, transactions, users};

    let db_connection = crate::establish_db_connection();

    let all_invites = invites.load::<Invite>(&db_connection)?;
    let all_users = users.load::<User>(&db_connection)?;
    let all_trips = trips.load::<Trip>(&db_connection)?;

    let mut trips_info: Vec<(Trip, Vec<User>, Vec<Transaction>)> =
        Vec::with_capacity(all_trips.len());

    for trip in all_trips {
        let trip_users = users::table
            .inner_join(relations::table)
            .select(users::all_columns)
            .filter(relations::trip_id.eq(trip.trip_id))
            .load::<User>(&db_connection)?;

        let trip_transactions = transactions::table
            .filter(transactions::trip_id.eq(&trip.trip_id))
            .load::<Transaction>(&db_connection)?;

        trips_info.push((trip, trip_users, trip_transactions));
    }

    let mut context = Context::new();
    context.insert("invites", &all_invites);
    context.insert("users", &all_users);
    context.insert("trips_info", &trips_info);
    context.insert("my_user", &admin_user);

    Ok(context)
}

pub async fn handle(
    data: web::Json<ActionWrapper>,
    id: Identity,
) -> Result<impl Responder, ServerError> {
    use schema::invites::dsl::*;
    use schema::relations::dsl::*;
    use schema::transactions::dsl::*;
    use schema::trips::dsl::*;
    use schema::users::dsl::*;
    use schema::{invites, relations, transactions, trips, users};

    match identity::verify_admin(&id) {
        Ok(_) => {}
        Err(IdentityError::UserNotAdmin) => {
            return Ok(dashboard::redirect());
        }
        Err(_) => {
            id.forget();
            return Ok(login::redirect());
        }
    };

    let db_connection = crate::establish_db_connection();

    match data.0 {
        ActionWrapper::AddInvite(i) => {
            if i.invite_id == "" {
                return Err(ServerError::new("Invalid invite"));
            }

            diesel::insert_into(invites::table)
                .values(&i)
                .execute(&db_connection)?;
        }
        ActionWrapper::DeleteInvite(i) => {
            diesel::delete(invites.find(i.invite_id)).execute(&db_connection)?;
        }
        ActionWrapper::AddTrip(t) => {
            if t.title == "" {
                return Err(ServerError::new("Invalid trip"));
            }

            diesel::insert_into(trips::table)
                .values(&t)
                .execute(&db_connection)?;
        }
        ActionWrapper::DeleteTrip(t) => {
            diesel::delete(trips.find(t.trip_id)).execute(&db_connection)?;
            diesel::delete(relations.filter(relations::trip_id.eq(t.trip_id)))
                .execute(&db_connection)?;
            diesel::delete(transactions.filter(transactions::trip_id.eq(t.trip_id)))
                .execute(&db_connection)?;
        }
        ActionWrapper::DeleteUser(u) => {
            if u.user_id == 1 {
                return Err(ServerError::new("Couldn't modify root"));
            }

            diesel::delete(users.find(u.user_id)).execute(&db_connection)?;
            diesel::delete(relations.filter(relations::user_id.eq(u.user_id)))
                .execute(&db_connection)?;
            diesel::delete(
                transactions
                    .filter(transactions::sender_id.eq(u.user_id))
                    .or_filter(transactions::recipient_id.eq(u.user_id)),
            )
            .execute(&db_connection)?;
        }
        ActionWrapper::AddRelation(r) => {
            users::table.find(r.user_id).first::<User>(&db_connection)?;
            trips::table.find(r.trip_id).first::<Trip>(&db_connection)?;

            diesel::insert_into(relations::table)
                .values(&r)
                .execute(&db_connection)?;
        }
        ActionWrapper::DeleteRelation(r) => {
            diesel::delete(
                relations
                    .filter(relations::user_id.eq(r.user_id))
                    .filter(relations::trip_id.eq(r.trip_id)),
            )
            .execute(&db_connection)?;

            diesel::delete(
                transactions
                    .filter(transactions::sender_id.eq(r.user_id))
                    .or_filter(transactions::recipient_id.eq(r.user_id))
                    .filter(transactions::trip_id.eq(r.trip_id)),
            )
            .execute(&db_connection)?;
        }
        ActionWrapper::ToggleAdmin(u) => {
            if u.user_id == 1 {
                return Err(ServerError::new("Couldn't modify root"));
            }

            let admin_state = users::table
                .find(u.user_id)
                .select(users::admin)
                .first::<bool>(&db_connection)?;

            diesel::update(users.find(u.user_id))
                .set(admin.eq(!admin_state))
                .execute(&db_connection)?;
        }
        ActionWrapper::DeleteTransaction(t) => {
            diesel::delete(transactions::table.find(t.transaction_id)).execute(&db_connection)?;
        }
        _ => {}
    };

    Ok(HttpResponse::Ok().finish())
}
