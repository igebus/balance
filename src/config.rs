use serde::Deserialize;
use std::fs::File;
use std::io::BufReader;

lazy_static! {
    pub static ref CONFIG: Config = {
        let args: Vec<String> = std::env::args().collect();
        match args.get(1) {
            Some(config_path) => {
                let config: Config = serde_json::from_reader(BufReader::new(
                    File::open(config_path).expect(&format!("File {} does not exist", config_path)),
                ))
                .unwrap();
                config
            }
            None => Config {
                socket_addr: "localhost:8080".to_string(),
                database_url: "balance.sqlite3".to_string(),
            },
        }
    };
}

#[derive(Deserialize, Clone)]
pub struct Config {
    pub socket_addr: String,
    pub database_url: String,
}
